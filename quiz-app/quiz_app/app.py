import json
import re
# import requests

from random import choice


from repositories.quiz import MockQuizRepository
from repositories.quizqa import MockQuizQARepository
from repositories.response import MockQuizUserResponseRepository
from db.mock_db import MockDatabse


def lambda_handler(event, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    # try:
    #     ip = requests.get("http://checkip.amazonaws.com/")
    # except requests.RequestException as e:
    #     # Send some context about this error to Lambda Logs
    #     print(e)

    #     raise e


    

    data = handle_request(event) or 'empty'
    print('data : ', data)

    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
        'body': json.dumps({
            'success': True,
            'data' : data
        }),
        "isBase64Encoded": False
    }


class PathNotFoundError(Exception):
    def __init__(self, message="Requested Path not found"):
        self.message = message
        super().__init__(self.message)

def resolve_route(end_point):
    quiz_regex = r'^\/quiz$'
    quiz_id_regex = r'^\/quiz\/\d+$'
    end_point = end_point[:-1] if end_point.endswith('/') else end_point

    if(re.findall(quiz_regex, end_point)):
        return 'quiz'
    elif(re.findall(quiz_id_regex, end_point)):
        return 'quiz_id'
    else:
        raise PathNotFoundError


def GET_handler(event):
    
    end_point = event.get('path', None)
    request_to = resolve_route(end_point=end_point)
    if request_to == 'quiz':
        return ('quiz', MockQuizRepository)
    elif request_to == 'quiz_id':
        return ('quiz_id', MockQuizQARepository)
    else:
        return None

    # return {'body':body, 'end_point':end_point}

def POST_handler(event):

    # body = event.get('body', None)
    body = {}
    try:
        print(event.get("body", {}))
        body = json.loads(event['body'])

    except:
        pass


    users = ('teacher', 'student')
    requested_user = body.get('user', None) or choice(users)

    end_point = event.get('path', None)
    request_to = resolve_route(end_point=end_point)

    # if request_to == 'quiz' and requested_user != 'student': # should use be capable to create quiz?
    if request_to == 'quiz':
        return ('quiz_repo', MockQuizRepository)
    elif request_to == 'quiz_id':
        if requested_user == 'student':
            return ('response_repo', MockQuizUserResponseRepository)
        else:
            return ('question_repo', MockQuizQARepository)
    else:
        return None

    # return {'body':body, 'end_point':end_point}

def handle_request(event):


    methods = ('GET', 'POST')
    # method = event.get('httpMethod', None)
    method = ''
    try:
        print(event.get("httpMethod", {}))
        method = event['httpMethod']

    except:
        pass


    print(event)
    if method not in methods:
        return None

    data = {}
    db = MockDatabse()

    if method == 'GET':
        name, handler = GET_handler(event)
        if name == 'quiz':
            data = handler(db=db).get()
            print('GET DATA', data)
        elif name == 'quiz_id':
            data = handler(db=db).get(quiz_id=1)
            print('GET DATA', data)
            

    elif method == 'POST':
        name, handler = POST_handler(event)
        quiz = {
            'created_by' : 1,
            'name' : 'CSE101',
            'description':'CT01'
        }
        quiz_qa = {
            'question' : 'Color of sky is?',
            'opt1' :'red',
            'opt2' :'green',
            'opt3' :'blue',
            'opt4' :'black',
            'ans' :'blue'
        }
        quiz_user_response = {
            'userId':1,
            'quizId':1,
            'quizQAId':1,
            'response':'blue'
        }
        if name == 'quiz_repo':
            data = handler(db=db).add(quiz=quiz)
            
        elif name == 'question_repo':
            data = handler(db=db).add(quiz_qa=quiz_qa)
        elif name == 'response_repo':
            data = handler(db=db).add(quiz_user_response=quiz_user_response)


    else:
        pass # handle more http methods later

    


    return data


'''
Working EndPoints:
curl -X GET "https://laiomqdsc1.execute-api.us-east-2.amazonaws.com/Prod/quiz" 
curl -X GET "https://laiomqdsc1.execute-api.us-east-2.amazonaws.com/Prod/quiz/1"
curl -X POST "https://laiomqdsc1.execute-api.us-east-2.amazonaws.com/Prod/quiz/" -d '{"user":"student"}'
curl -X POST "https://laiomqdsc1.execute-api.us-east-2.amazonaws.com/Prod/quiz/" -d '{"user":"teacher"}'
curl -X POST "https://laiomqdsc1.execute-api.us-east-2.amazonaws.com/Prod/quiz/1" -d '{"user":"student"}'
curl -X POST "https://laiomqdsc1.execute-api.us-east-2.amazonaws.com/Prod/quiz/1" -d '{"user":"teacher"}'

'''
