import datetime
from dataclasses import dataclass


@dataclass
class Quiz:
    created_by: int  # userid
    name: str = 'a quiz model'
    description: str = 'Testing QUIZ model'
    question_count: int = 1
    Id: int = None
    created_at: datetime.datetime = datetime.datetime.now()

    def db_sql_params(self):
        return self.created_by, self.name, self.description, self.question_count, str(self.created_at)

    @staticmethod
    def create_quiz(quiz_data):
        date_format = "%Y-%m-%d %H:%M:%S.%f"
        quiz_Id, created_by, name, description, question_count, created_at = quiz_data
        return Quiz(Id=quiz_Id, created_by=created_by, name=name, description=description,
                    question_count=question_count,
                    created_at=datetime.datetime.strptime(created_at, date_format))



@dataclass
class QuizQA:
    quiz_id: int
    question: str
    opt1: str
    opt2: str
    opt3: str
    opt4: str
    ans: str
    Id: int = None

    def db_sql_params(self):
        return self.quiz_id, self.question, self.opt1, self.opt2, self.opt3, self.opt4, self.ans

    @staticmethod
    def create_quiz_qa(quiz_qa_data):
        Id, quiz_id, question, opt1, opt2, opt3, opt4, ans = quiz_qa_data
        return QuizQA(Id=Id, quiz_id=quiz_id, question=question, opt1=opt1, opt2=opt2, opt3=opt3, opt4=opt4, ans=ans)


@dataclass
class QuizUserResponse:
    userId: int  # userID
    quizId: int
    quizQAId: int
    response: str
    Id: int = None

    def db_sql_params(self):
        return self.userId, self.quizId, self.quizQAId, self.response

    @staticmethod
    def create_quiz_user_response(user_response):
        Id, user_Id, quiz_Id, quiz_Qa_Id, response = user_response
        return QuizUserResponse(Id=Id, userId=user_Id, quizId=quiz_Id, quizQAId=quiz_Qa_Id, response=response)


@dataclass
class Score:
    userId: int
    quizId: int
    score: int = 0
    Id: int = None

    def db_sql_params(self):
        return self.userId, self.quizId, self.score

    @staticmethod
    def create_score(score_data):
        score_id, user_id, quiz_id, score = score_data
        return Score(userId=user_id, quizId=quiz_id, score=score, Id=score_id)
